package conversor;
import java.util.Scanner;

public class Test {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        Conversor c1 = new Conversor();
        double valor = 10.0;
        String hacia = "";
        String desde = "";

        System.out.println("Por favor introduzca el valor que desea convertir:");
        valor = s.nextDouble();
        System.out.println("Ahora introduzca el tipo de valor que acaba de introducir: ");
        System.out.println("Pista:\n km/h \n m/s \n m/h \n yd/s \n");
        desde = s.next();
        System.out.println("Ahora introduzca el tipo de valor que al que desea convertir: ");
        System.out.println("Pista:\n km/h \n m/s \n m/h \n yd/s \n");
        hacia = s.next();
        s.close();

        System.out.println(desde + " " + hacia);

        double resultado = c1.convierte(valor, desde, hacia);

        System.out.printf("%.2f %s equivalen a %.2f %s", valor, desde, resultado, hacia);

    }

}