package conversor;

public class Conversor {

    public double convierte(double valor, String desde, String hacia) {

        double resultado = 0.0;

        if (desde.equals("km/h")) {
            if (hacia.equals("mi/h")) {
                resultado = valor / 1.609;
            } else if (hacia.equals("m/s")) {
                resultado = valor / 3.6;
            } else if (hacia.equals("yd/s")) {
                resultado = valor / 3.292;
            }
        } else if (desde.equals("mi/h")) {
            if (hacia.equals("km/h")) {
                resultado = valor * 1.609;
            } else if (hacia.equals("m/s")) {
                resultado = valor / 2.237;
            } else if (hacia.equals("yd/s")) {
                resultado = valor / 2.045;
            }
        } else if (desde.equals("m/s")) {
            if (hacia.equals("km/h")) {
                resultado = valor * 3.6;
            } else if (hacia.equals("m/h")) {
                resultado = valor * 2.237;
            } else if (hacia.equals("yd/s")) {
                resultado = valor * 1.094;
            }
        } else if (desde.equals("yd/s")) {
            if (hacia.equals("km/h")) {
                resultado = valor * 3.292;
            } else if (hacia.equals("m/h")) {
                resultado = valor * 2.045;
            } else if (hacia.equals("m/s")) {
                resultado = valor / 1.094;
            }
        }

        return resultado;

    }

}