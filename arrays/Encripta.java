package arrays;

/**
 * Encripta1
 */
public class Encripta {

    public static String encripta1(String s){
        String resultado = "";

        for ( int i = 0; i < s.length(); i++ ) {
            char c = s.charAt(i);
            if ( c == 'a' ) c = '1';
            if ( c == 'e' ) c = '2';
            if ( c == 'i' ) c = '3';
            if ( c == 'o' ) c = '4';
            if ( c == 'u' ) c = '5';
            resultado = resultado + c;
        }
        
        return resultado;
    }
    
}