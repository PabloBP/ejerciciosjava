package arrays;

/**
 * Palindromo
 */
public class Palindromo {

    public static String palindromo(String s){
        String resultado = "";

        char c = s.charAt(s.length()-1);
        c = Character.toUpperCase(c);
        resultado = resultado + c;

        for ( int i = s.length()-2; i > 0; i--  ) {
            c = s.charAt(i);
            resultado = resultado + c;
        }

        c = s.charAt(0);
        c = Character.toLowerCase(c);
        resultado = resultado + c;

        return resultado;
    }

    
}