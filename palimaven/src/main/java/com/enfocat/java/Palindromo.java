package com.enfocat.java;

/**
 * Palindromo
 */
public class Palindromo {

    public static String giraPalabra(String s) {
        char[] letras = s.toCharArray();
        String resultado = "";

        for ( int i = letras.length-1; i >= 0; i-- ) {
            resultado = resultado + letras[i];
        }

        return resultado;
    }
    
}