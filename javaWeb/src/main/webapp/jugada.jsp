<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="enfocat.web.Pipati" %>
<%@page import="enfocat.web.HtmlFactory" %>
<%
HtmlFactory factory = new HtmlFactory();
int jugada_usuario = Integer.parseInt(request.getParameter("jugada")); //1
Pipati resultado = Pipati.partida(jugada_usuario); //2
%>
<!DOCTYPE html>
<html lang="es-ES">

<head>
    <meta charset="utf-8">
    <title>Java demo</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
    <div class="titulo">
        <%= factory.titulo(resultado.respuesta) %>
    </div>
    &nbsp;
    <div class="titulo3">
        <%= factory.imagen(resultado.jugada_user) %>
        &nbsp;
        <h1>VS</h1>
        &nbsp;
        <%= factory.imagen(resultado.jugada_pc) %>
    </div>
    
    <a href="index.jsp">Volver a jugar</a>
</body>

</html>