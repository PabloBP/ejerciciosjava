<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="enfocat.web.HtmlFactory" %>
<%
  HtmlFactory factory = new HtmlFactory();
%>
<!DOCTYPE html>
<html lang="es-ES">

<head>
  <meta charset="utf-8">
  <title>Java demo</title>
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
  <div class="titulo">
    <%= factory.titulo("Re: Piedra, Papel, Tijera Ultimate Edition HD Remaster") %>
    <img class="indice" src="images/ppt.jpg" alt="Castañas">
  </div>
  &nbsp;
  <div class="titulo2">
  <h3>Por favor selecciona tu siguiente jugada:</h3>
  <h4>Pista: No deberias saber jugar ya a piedra papel tijeras?</h4>
  <ul>
    <li><a href="jugada.jsp?jugada=1">Piedra</a></li>
    <li><a href="jugada.jsp?jugada=2">Papel</a></li>
    <li><a href="jugada.jsp?jugada=3">Tijera</a></li>
  </ul>
  </div>
</body>

</html>