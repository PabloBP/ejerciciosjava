package enfocat.web;

/**
 * HtmlFactory
 */
public class HtmlFactory {
    public String titulo(String contenido) {
        String plantilla = "<h1>%s<h1>";
        return String.format(plantilla, contenido);
    }

    public String imagen(int titulo) {
        String plantilla = "<img src=\"images/%d.jpg\" alt=\"Castañas\">";
        return String.format(plantilla, titulo);
    }
}