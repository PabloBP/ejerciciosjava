package enfocat.web;

import java.util.Random;

public class Pipati {
    private static String[] jugadas = { "Piedra", "Papel", "Tijera" };

    public int jugada_user;
    public int jugada_pc;
    public String respuesta;

    public Pipati(int jugada_user, int jugada_pc, String respuesta) {
        this.jugada_user = jugada_user;
        this.jugada_pc = jugada_pc;
        this.respuesta = respuesta;
    }

    public static Pipati partida(int jugada_usuario) {
        String respuesta = "";
        Random rnd = new Random();
        int jugada_ordenador = rnd.nextInt(3) + 1;
        if (jugada_usuario == jugada_ordenador) {
            respuesta = String.format("Empate! yo también he sacado %s", jugadas[jugada_usuario - 1]);
        } else if (jugada_usuario == 1 && jugada_ordenador == 2) {
            respuesta = String.format("Pierdes con %s, yo he sacado %s", jugadas[jugada_usuario - 1],
                    jugadas[jugada_ordenador - 1]);
        } else if (jugada_usuario == 1 && jugada_ordenador == 3) {
            respuesta = String.format("Ganas con %s, yo he sacado %s", jugadas[jugada_usuario - 1],
                    jugadas[jugada_ordenador - 1]);
        } else if (jugada_usuario == 2 && jugada_ordenador == 1) {
            respuesta = String.format("Ganas con %s, yo he sacado %s", jugadas[jugada_usuario - 1],
                    jugadas[jugada_ordenador - 1]);
        } else if (jugada_usuario == 2 && jugada_ordenador == 3) {
            respuesta = String.format("Pierdes con %s, yo he sacado %s", jugadas[jugada_usuario - 1],
                    jugadas[jugada_ordenador - 1]);
        } else if (jugada_usuario == 3 && jugada_ordenador == 1) {
            respuesta = String.format("Pierdes con %s, yo he sacado %s", jugadas[jugada_usuario - 1],
                    jugadas[jugada_ordenador - 1]);
        } else if (jugada_usuario == 3 && jugada_ordenador == 2) {
            respuesta = String.format("Ganas con %s, yo he sacado %s", jugadas[jugada_usuario - 1],
                    jugadas[jugada_ordenador - 1]);
        }
        return new Pipati(jugada_usuario, jugada_ordenador, respuesta);
    }

}