package motos;

public class Test {

    public static void main(String[] args) {
        
        Moto m1 = new Moto("Honda", "GoldWing", 120, 3000);
        Moto m2 = new Moto("Ducati", "Multistrada", 160, 17000);
        Moto m3 = new Moto("Yamaha", "Tmax", 225, 12000);
        Moto m4 = new Moto("Suzuki", "VStrom 650", 70, 8000);
        Moto[] motos = { m1, m2, m3, m4};

        MotoUtil.informe(motos);
        MotoUtil.compara(m1, m2);


    }
}