package motos;

/**
 * MotoUtil
 */
public class MotoUtil {

    public static Moto masPotente(Moto[] motos) {
        Moto mPotente = motos[0];

        for ( int i = 1; i < motos.length; i++ ) {
            if ( mPotente.getPotencia() < motos[i].getPotencia() ) {
                mPotente = motos[i];
            }
        }

        return mPotente;
    }

    public static Moto masCara(Moto[] motos) {
        Moto mCara = motos[0];

        for ( int i = 1; i < motos.length; i++ ) {
            if ( mCara.getPrecio() < motos[i].getPrecio() ) {
                mCara = motos[i];
            }
        }

        return mCara;
    }

    public static void informe(Moto[] motos){
        Moto mCara = masCara(motos);
        Moto mPotente = masPotente(motos);
        double precioFinal = mCara.getPrecio() + mPotente.getPrecio();

        System.out.println("La moto mas cara de todas es la " + 
        mCara.getMarca() + " (" + mCara.getPrecio() + " eur)");

        System.out.println("La moto mas potente de todas es la " + 
        mPotente.getMarca() + " (" + mPotente.getPotencia() + " cv)");

        System.out.println("El precio conjunto de las motos es de " + precioFinal + " eur");
        
    }

    public static void compara(Moto m1, Moto m2){
        // Potencia
        int potenciaDiferencia = 0;
        if ( m1.getPotencia() < m2.getPotencia() ) {
            potenciaDiferencia = m2.getPotencia() - m1.getPotencia();
            System.out.println("La " + m2.getMarca() + " " + m2.getModelo() + 
            " tiene " + potenciaDiferencia + "cv mas que la " + 
            m1.getMarca() + " " + m1.getModelo());
        } else {
            potenciaDiferencia = m1.getPotencia() - m2.getPotencia();
            System.out.println("La " + m1.getMarca() + " " + m1.getModelo() + 
            " tiene " + potenciaDiferencia + "cv mas que la " + 
            m2.getMarca() + " " + m2.getModelo());
        }

        // Precio
        double precioDiferencia = 0;
        if ( m1.getPrecio() < m2.getPrecio() ) {
            precioDiferencia = m2.getPrecio() - m1.getPrecio();
            System.out.println("La " + m2.getMarca() + " " + m2.getModelo() + 
            " es " + precioDiferencia + "eur mas cara que la " + 
            m1.getMarca() + " " + m1.getModelo());
        } else {
            precioDiferencia = m1.getPrecio() - m2.getPrecio();
            System.out.println("La " + m1.getMarca() + " " + m1.getModelo() + 
            " es " + precioDiferencia + "eur mas cara que la " + 
            m2.getMarca() + " " + m2.getModelo());
        }
    }
    
}