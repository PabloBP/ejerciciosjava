package args;

public class InfoNums {

    public static void main(String[] args) {
        int tamanio = args.length;
        int total = 0;
        int mayor = 0, menor = 999;
        int media = 0;

        for (String s : args) {
            int num = Integer.parseInt(s);
            total += num;

            if (num > mayor) {
                mayor = num;
            }
            if (num < menor) {
                menor = num;
            }

            media = total / args.length;

        }
        System.out.println(tamanio + " numeros introducidos\n" + "numero mayor: " + mayor + "\n" + "numero menor: "
                + menor + "\n" + "media arimetica: " + media);

    }

}