package juegos;
import java.util.Random;
import java.util.Scanner;

public class PiedraPapelTijera {

    // 0 piedra
    // 1 papel
    // 2 tijera

    public static void juego() {
        Random random = new Random();
        Scanner s = new Scanner(System.in);
        int nPartidas = 5;
        int contador = 0;
        int jugador = 0;
        int ganadas = 0, empates = 0, perdidas = 0;
        boolean[][] resultados = new boolean[3][3];
        resultados[0][2] = true; resultados[1][0] = true; resultados[2][1] = true;


        while ( contador < nPartidas ) {
            int ordenador = random.nextInt(3);
            System.out.println("Por favor introduzca su siguiente jugada:\nPista:\n" + 
            "0 Piedra \n1 Papel \n2 Tijera\n");
            jugador = s.nextInt();

            System.out.println("\nJugador: " + jugador + "\nOrdenador: " + ordenador + "\n");

            if (jugador == ordenador) {
                System.out.println("Empate!\n");
                empates++;
            } else if (resultados[jugador][ordenador]) {
                System.out.println("Has ganado!\n");
                ganadas++;
            } else {
                System.out.println("Has perdido!\n");
                perdidas++;
            }

            ++contador;
        }

        System.out.printf("Partidas ganadas: %d \nPartidas perdidas: %d \nPartidas empatadas: %d \n\n",
        ganadas, perdidas, empates);

        if (ganadas > perdidas && ganadas > empates || ganadas > perdidas && ganadas == empates ) {
            System.out.println("Has ganado la partida! Grande!");
        } else if (ganadas < perdidas && perdidas > empates || ganadas < perdidas && perdidas == empates) {
            System.out.println("Has perdido... Mala suerte");
        } else if (empates > perdidas && ganadas < empates) {
            System.out.println("Has empatado muchas veces, eres mago?");
        } else if (ganadas == perdidas) {
            System.out.println("Has empatado en victorias y derrotas, eres mago?");
        }

        s.close();

    }

    
}