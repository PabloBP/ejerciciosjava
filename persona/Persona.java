package persona;

public class Persona {

    private String nombre;
    private String email;
    private int edad;
    private int id;

    public Persona(String nombre, String email, int edad, int id) {
        this.nombre = nombre;
        this.email = email;
        this.edad = edad;
        this.id = id;
    }

    // Getters and Setters

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


}