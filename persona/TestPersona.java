package persona;

public class TestPersona {

    public static void main(String[] args) {
        
        Persona p1 = new Persona("Pablo", "jaja@jaja.com", 23, 1);
        Persona p2 = new Persona("Estefanie", "jaja@jaja.com", 22, 1);

        PersonaController pc1 = new PersonaController();

        pc1.muestraPersona(p1);
        pc1.muestraPersona(p2);
        pc1.comparaPersona(p1, p2);



    }

}