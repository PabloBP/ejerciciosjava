package persona;

public class PersonaController {

    public void muestraPersona(Persona p1) {
        System.out.println(p1.getNombre() + " tiene " + p1.getEdad() + " a\u00f1os y su email es " + p1.getEmail());
    }

    public void comparaPersona(Persona p1, Persona p2) {
        if ( p1.getEdad() < p2.getEdad() ) {
            System.out.println(p2.getNombre() + " (" + p2.getEdad() + ") es mayor que " +
            p1.getNombre() + " (" + p1.getEdad() + ")");
        } else {
            System.out.println(p1.getNombre() + " (" + p1.getEdad() + ") es mayor que " +
            p2.getNombre() + " (" + p2.getEdad() + ")");
        }
    }

}