package motos2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Scanner;

public class Filtro {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Archivo no especificado.");
            return;
        }

        String archivo = args[0].toLowerCase();
        String marca;
        Scanner s = new Scanner(System.in);
        System.out.println("Por favor introduzca su marca a filtrar");
        marca = s.nextLine();
        s.close();
        String marca2 = marca.toLowerCase() + ".csv";
        File fin = new File(archivo);
        File salida = new File("motos2",marca2);

        try (FileReader fr = new FileReader(fin);
                BufferedReader br = new BufferedReader(fr);
                FileWriter fw = new FileWriter(salida);
                BufferedWriter bw = new BufferedWriter(fw);) {
            String line;
            do {
                line = br.readLine();
                if (line != null) {
                    if (line.indexOf(marca) >= 0) {
                        bw.write(line);
                        bw.newLine();
                    }
                    
                }

            } while (line != null);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}