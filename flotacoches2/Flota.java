package flotacoches2;

import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;

public class Flota {

    private static Map<Integer, Coche> coches = new TreeMap<Integer, Coche>();
    private static String[] marcas = {"Renault", "Dacia", "Citroen", "Seat"};
    private static String[] colores = {"rojo", "blanco", "negro"};
    
    public static void generaCoches(int num) {
        Random r = new Random();
        int marcasSize = marcas.length, coloresSize = colores.length;

        for ( int i = 0; i < num; i++ ) {
            int anios = ThreadLocalRandom.current().nextInt(1999, 2019);
            Coche c = new Coche(marcas[r.nextInt(marcasSize)], colores[r.nextInt(coloresSize)], r.nextInt(150000), anios);
            coches.put(i,c);
        }
    }

    public static void resumen() {
        int contador =  0, indiceMax = 0, contadorMax = 0;

        System.out.println(coches.size() + " coches en lista");

        // Marcas
        // El primer for sera de las marcas del coche, el segundo es del arraylist

        for ( int i = 0; i < marcas.length; i++ ) {
            for ( int j = 0; j < coches.size(); j++ ) {
                if ( marcas[i] == coches.get(j).getMarca() ) {
                    contador++;
                } 
            }
            if ( contador > contadorMax ) {
                contadorMax = contador;
                indiceMax = i;
            }
            contador = 0;
        } 
        System.out.println("La marca mas comun es " + marcas[indiceMax] + " y tiene " + contadorMax + " coches");

        // Colores
        // El primer for sera de las marcas del coche, el segundo es del arraylist
        for ( int i = 0; i < colores.length; i++ ) {
            for ( int j = 0; j < coches.size(); j++ ) {
                if ( colores[i] == coches.get(j).getColor() ) {
                    contador++;
                } 
            }
            if ( contador > contadorMax ) {
                contadorMax = contador;
                indiceMax = i;
            }
            contador = 0;
        } 
        System.out.println("El color mas frecuente es " + colores[indiceMax] + " con " + contadorMax + " coches");

        contador = 0;
        // Kilometros
        for ( int i = 0; i < coches.size(); i++ ) {
            if ( coches.get(i).getKm() > 100000 ) contador++;
        }
        System.out.println(contador + " coches tienen mas de 100000km");
    }

    public static void consultaKm(int minkm){
        for ( int i = 0; i < coches.size(); i++ ) {
            if ( coches.get(i).getKm() >= minkm ) {
                coches.get(i).toString();
            }
        }
    }

    public static void consultaColor(String color){
        for ( int i = 0; i < coches.size(); i++ ) {
            if ( coches.get(i).getColor().equals(color) ) {
                coches.get(i).toString();
            }
        }
    }

    public static void consultaMarca(String marca){
        for ( int i = 0; i < coches.size(); i++ ) {
            if ( coches.get(i).getMarca().equals(marca) ) {
                coches.get(i).toString();
            }
        }
    }

    public static void consultaMultiple(String color, String marca, int minkm){
        for ( int i = 0; i < coches.size(); i++ ) {
            if ( coches.get(i).getMarca().equals(marca) ) {
                if ( coches.get(i).getColor().equals(color) ) {
                    if ( coches.get(i).getKm() >= minkm ) {
                        coches.get(i).toString();
                    }
                }
            }
        }
    }

    public static void listaMarcas(){
        int contador = 0;
        for ( int i = 0; i < marcas.length; i++ ) {
            for ( int j = 0; j < coches.size(); j++ ) {
                if ( marcas[i] == coches.get(j).getMarca() ) {
                    contador++;
                } 
            }
            System.out.println("La marca " + marcas[i] + " tiene " + contador + " coches.");
            contador = 0;
        } 
    }

    public static void listaColores(){
        int contador = 0;
        for ( int i = 0; i < colores.length; i++ ) {
            for ( int j = 0; j < coches.size(); j++ ) {
                if ( colores[i] == coches.get(j).getColor() ) {
                    contador++;
                } 
            }
            System.out.println(contador + " coches son de color " + colores[i]);
            contador = 0;
        } 
    }

}