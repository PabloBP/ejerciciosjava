package flotacoches2;

public class Coche {
    private String marca;
    private String color;
    private int km;
    private int anyo;

    public Coche(String marca, String color, int km, int anyo) {
        this.marca = marca;
        this.color = color;
        this.km = km;
        this.anyo = anyo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getAnyo() {
        return anyo;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }

    @Override
    public String toString() {
        return "Marca: " + marca + " Color: " + color + 
        " Anio: " + anyo + " Km total: " + km;
    }
    
    
}