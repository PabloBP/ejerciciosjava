package keyboard;

import java.util.Random;
import java.util.Scanner;

public class Adivina {
    public static void main(String[] args) {

        Random random = new Random();
        Scanner sa = new Scanner(System.in);
        int limite = 5;
        int incognita = random.nextInt(limite) + 1;
        int num = 0;
        boolean encontrado = false;
        int contador = 0;

        while ( !encontrado ) {
            System.out.println("Introduzca un numero: ");
            try {
            num = sa.nextInt();
            } catch ( Exception e ) {
                System.out.println("***Datos incorrectos***");
                sa.next();
            }
            if ( num == incognita ) {
                System.out.println("Has acertado!");
                ++contador;
                encontrado = true;
            } else {
                System.out.println("Has fallado!");
                ++contador;
            }

        }

        System.out.printf("Numero de intentos: %d \n", contador);
        sa.close();

    }

}