package keyboard;

import java.util.Scanner;

public class InfoNumsK {

    public static void main(String[] args) {

        Scanner sa = new Scanner(System.in);

        int tamanio = 0;
        int total = 0;
        int mayor = 0, menor = 0;
        int media = 0;
        int num = 0;

        System.out.println("Por favor introduzca un numero.\nSi desea salir del programa ponga un cero.");
        num = sa.nextInt();

        if ( num != 0 ) {
            menor = 999;
        }

        while (num != 0) {

            total += num;

            if (num > mayor) {
                mayor = num;
            }
            if (num < menor) {
                menor = num;
            }

            tamanio++;

            System.out.println("Por favor introduzca un numero.\nSi desea salir del programa ponga un cero.");
            num = sa.nextInt();

        }

        if (tamanio > 0 && total > 0) {
            media = total / tamanio;
        }

        System.out.println(tamanio + " numeros introducidos\n" + "numero mayor: " + mayor + "\n" + "numero menor: "
                + menor + "\n" + "media arimetica: " + media);
        sa.close();
    }

}