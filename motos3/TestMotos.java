import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringWriter;
import java.io.BufferedWriter;
import java.io.File;

public class TestMotos {

    public static void main(String[] args) {
        Gson gson = new Gson();
        File f = new File("motos.json");
        int len;
        int size = 100;
        char[] buffer = new char[size];
        String contenido = "";

        try (FileReader fr = new FileReader(f);
                BufferedReader br = new BufferedReader(fr);
                StringWriter sw = new StringWriter();) {

            while ((len = br.read(buffer, 0, size)) > -1) {
                sw.write(buffer, 0, len);
            }

            contenido = sw.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Moto[] importado = gson.fromJson(contenido, Moto[].class);
        double precioMedio = 0;
        int contador = 0;

        for (Moto x : importado) {
            if (x.model.indexOf("SCOOPY") >= 0) {
                contador++;
                precioMedio = precioMedio + (double) x.preu;
            }
        }

        precioMedio = precioMedio / (double) contador;

        System.out.printf("Hay %d motos Scoopy, su precio medio es %.2f eur", contador, precioMedio);

    }

}