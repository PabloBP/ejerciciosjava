package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;

public class Datos {
    
    private static List<Contacto> contactos = new ArrayList<Contacto>();
    private static List<LLamada> llamadas = new ArrayList<LLamada>();
    private static int ultimoContacto = 0;
    private static int ultimaLLamada = 0;

    static {
        contactos.add(new Contacto(1,"ana", "ana@gmail.com","Barcelona","12345678"));
        contactos.add(new Contacto(2,"joan", "joan@gmail.com","Barcelona","12345678"));
        llamadas.add(new LLamada(1,"1996-06-06","notas", 1));
        llamadas.add(new LLamada(2,"2000-11-19","notas", 2));
        ultimoContacto = 2;
        ultimaLLamada = 2;
    }



    public static Contacto newContacto(Contacto cn){
        ultimoContacto++;
        cn.setId(ultimoContacto);
        contactos.add(cn);
        return cn;
    }

    public static LLamada newLLamada(LLamada cn){
        ultimaLLamada++;
        cn.setId(ultimaLLamada);
        llamadas.add(cn);
        return cn;
    }

    public static Contacto getContactoId(int id){
        for (Contacto cn : contactos){
            if (cn.getId()==id){
                return cn;
            }
        }
        return null;
    }

    public static LLamada getLLamadaId(int id){
        for (LLamada cn : llamadas){
            if (cn.getId()==id){
                return cn;
            }
        }
        return null;
    }

    public static boolean updateContacto(Contacto cn){
        boolean updated = false;
        for (Contacto x : contactos){
            if (cn.getId()==x.getId()){
                //x = cn;
                int idx = contactos.indexOf(x);
                contactos.set(idx,cn);
                updated=true;
            }
        }
        return updated;
    }

    public static boolean updateLLamada(LLamada cn){
        boolean updated = false;
        for (LLamada x : llamadas){
            if (cn.getId()==x.getId()){
                //x = cn;
                int idx = llamadas.indexOf(x);
                llamadas.set(idx,cn);
                updated=true;
            }
        }
        return updated;
    }

    public static boolean deleteContactoId(int id){
        boolean deleted = false;
        
        for (Contacto x : contactos){
            if (x.getId()==id){
                contactos.remove(x);
                deleted=true;
                break;
            }
        }
        return deleted;
    }

    public static boolean deleteLlamadaId(int id){
        boolean deleted = false;
        
        for (LLamada x : llamadas){
            if (x.getId()==id){
                llamadas.remove(x);
                deleted=true;
                break;
            }
        }
        return deleted;
    }

    public static List<Contacto> getContactos() {
        return contactos;
    }

    public static List<LLamada> getLLamadas() {
        return llamadas;
    }

}

