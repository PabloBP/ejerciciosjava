package com.enfocat.mvc;

import java.util.List;

public class LLamadaControl {

    static final String MODO = "db"; // db per bdd

    // getAll devuelve la lista completa
    public static List<LLamada> getAll(){
        if (MODO.equals("db")) {
            return DBDatos.getLlamadas();
        } else {
            return Datos.getLLamadas();
        }
    }

    public static List<LLamada> getEspecifico(int id){
        if (MODO.equals("db")) {
            return DBDatos.getLlamadas(id);
        } else {
            return null;
        }
    }

    //getId devuelve un registro
    public static LLamada getId(int id){
        if (MODO.equals("db")) {
            return DBDatos.getLlamadaId(id);
        } else {
            return Datos.getLLamadaId(id);
        }
    }
   
    //save guarda un LLamada
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(LLamada cn) {

        if (MODO.equals("db")) {
            if (cn.getId() > 0) {
                DBDatos.updateLLamada(cn);
            } else {
                DBDatos.newLLamada(cn);
            }
        } else {
            if (cn.getId() > 0) {
                Datos.updateLLamada(cn);
            } else {
                Datos.newLLamada(cn);
            }
        }
        
    }

    // size devuelve numero de Contactos
    public static int size() {
        if (MODO.equals("db")) {
            return DBDatos.getLlamadas().size();
        } else {
            return Datos.getLLamadas().size();
        }
    }


    // removeId elimina Contacto por id
    public static void removeId(int id){
        if (MODO.equals("db")) {
            DBDatos.deleteLLamadaId(id);
        } else {
            Datos.deleteLlamadaId(id);
        }
    }
    
}