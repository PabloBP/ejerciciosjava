package com.enfocat.mvc;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class LLamada {
    private int id;
    private Date fecha;
    private String notas;
    private int contacto;

    // Sin ID
    public LLamada(String fecha, String notas, int contacto) {
        this.id = 0;
        try {
            this.fecha = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.notas = notas;
        this.contacto = contacto;
    }

    // Con ID
    public LLamada(int id, String fecha, String notas, int contacto) {
        this.id = id;
        try {
            this.fecha = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.notas = notas;
        this.contacto = contacto;
    }

    public LLamada(int id, Date fecha, String notas, int contacto) {
        this.id = id;
        this.fecha = fecha;
        this.notas = notas;
        this.contacto = contacto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public int getContacto() {
        return contacto;
    }

    public void setContacto(int contacto) {
        this.contacto = contacto;
    }

}