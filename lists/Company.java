package lists;

public class Company {
    private String name;
    private int year;

    @Override
    public int hashCode() {
        return this.name.hashCode() * this.year;
    }

    @Override
    public boolean equals(Object obj) {
        Company otra = (Company) obj;
        return this.name.equals(otra.name) && this.year == otra.year;
    }

    public Company(String name, int year) {
        this.name = name;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    
    
}