package lists;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ListTest {
    public static void main(String[] args) {
        List<Integer> numeros = Arrays.asList(1, 2, 3, 4, 5);
        System.out.println(numeros.get(1));

        AtomicInteger x = new AtomicInteger(0);

        for ( int numero : numeros ) {
            if (numero%2!=0) {
                x.addAndGet(numero);
            }
        }
        

        
    }
}