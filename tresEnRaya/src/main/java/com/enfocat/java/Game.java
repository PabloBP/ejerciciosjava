package com.enfocat.java;
import java.util.Scanner;
import java.util.Random;

public class Game {

    public static int[] map = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    public static int[][] winners = {
        new int[] {0,1,2},
        new int[] {3,4,5},
        new int[] {6,7,8},
        new int[] {0,3,6},
        new int[] {1,4,7},
        new int[] {2,5,8},
        new int[] {0,4,8},
        new int[] {2,4,6}
        };


    public void setMap(int posicion, int jugador) {
        map[posicion] = jugador;
    }

    public int getMap(int posicion) {
        return map[posicion];
    }

    public void draw() {
        int contador = 1;

        for (int i = 0; i < map.length; i++) {
            if (map[i] == 0) {
                System.out.print("- ");
            } else if (map[i] == 1) {
                System.out.print("X ");
            } else if (map[i] == 2) {
                System.out.print("O ");
            }
            if (contador >= 3) {
                System.out.print("\n");
                contador = 0;
            }
            contador++;
        }
    }

    public int numZeros() {
        int contador = 0;

        for (int i = 0; i < map.length; i++) {
            if (map[i] == 0) {
                contador++;
            }
            
        }
        return contador;
    }

    public int winner() {
        int a = 0, b = 0, c = 0;
        int resultado = 0;
        for ( int i = 0; i < winners.length; i++ ) {
            a = winners[i][0]; b = winners[i][1]; c = winners[i][2]; 
            if ( map[a] == map[b] && map[a] == map[c] && map[a] == 1) {
                resultado = 1;
                break;
            } else if ( map[a] == map[b] && map[a] == map[c] && map[a] == 2 ) {
                resultado = 2;
                break;
            }
        }
        return resultado;
    }

    public int jugadaAI() {
        int a = 0, b = 0, c = 0;
        int movimiento = 10;
        Random r = new Random();

        // Completar jugada
        for ( int i = 0; i < winners.length; i++ ) {
            a = winners[i][0]; b = winners[i][1]; c = winners[i][2]; 
            if ( map[a] == map[b] && getMap(c) == 0 && map[a] == 2 ) {
                return c;
            } else if ( map[a] == map[c] && getMap(b) == 0 && map[a] == 2 ) {
                return b;
            } else if ( map[b] == map[c] && getMap(a) == 0 && map[b] == 2 ) {
                return a;
            }
        }

        a = 0; b = 0; c = 0;
        // Parar jugada del jugador valga la redundancia
        for ( int i = 0; i < winners.length; i++ ) {
            a = winners[i][0]; b = winners[i][1]; c = winners[i][2]; 
            if ( map[a] == map[b] && getMap(c) == 0 && map[a] == 1 ) {
                return c;
            } else if ( map[a] == map[c] && getMap(b) == 0 && map[a] == 1 ) {
                return b;
            } else if ( map[b] == map[c] && getMap(a) == 0 && map[b] == 1 ) {
                return a;
            }
        }
        // Jugada aleatoria en caso de que los otros casos no se den
        movimiento = r.nextInt(9);
        while ( getMap(movimiento) != 0 ) {
            movimiento = r.nextInt(9);
        }

        return movimiento;
    }

    public void play() {
        Scanner s = new Scanner(System.in);
        int victoria = 0, turnoJugador = 0, turnoPC = 0;

        draw();

        while ( numZeros()>0 && victoria == 0 ) {
            System.out.println("Turno del jugador: \nIntroduzca un numero del 0-8");
            turnoJugador = s.nextInt();
            while ( getMap(turnoJugador) != 0  || turnoJugador > 8 || turnoJugador < 0) {
                System.out.println("Celda invalida, pruebe otra vez");
                turnoJugador = s.nextInt();
            }
            setMap(turnoJugador, 1);
            draw();
            victoria = winner();

            if ( numZeros()>0 && victoria == 0 ) {
                System.out.println("Turno del ordenador");
                turnoPC = jugadaAI();
                setMap(turnoPC, 2);
                draw();
                victoria = winner();
            } 
        }

        if ( victoria == 1 ) {
            System.out.println("Has ganado!");
        } else if ( victoria == 2 ) {
            System.out.println("Has perdido...");
        } else {
            System.out.println("Empate!");
        }

        s.close();

    }

}